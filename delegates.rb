require 'uri'

class CustomDelegate
    attr_accessor :context

    def deserialize_meta_identifier(meta_identifier)
    end

    def serialize_meta_identifier(components)
    end

    def pre_authorize(options = {})
        true
    end

    def authorize(options = {})
        true
    end

    def extra_iiif2_information_response_keys(options = {})
        {}
    end

    def extra_iiif3_information_response_keys(options = {})
        {}
    end

    def source(options = {})
        identifier = context['identifier']
        if identifier =~ URI::regexp
            return 'HttpSource'
        else 
            return 'FilesystemSource'
        end
    end

    def azurestoragesource_blob_key(options = {})
    end

    def filesystemsource_pathname(options = {})
    end

    def httpsource_resource_info(options = {})
    end

    def jdbcsource_database_identifier(options = {})
    end

    def jdbcsource_media_type(options = {})
    end

    def jdbcsource_lookup_sql(options = {})
    end

    def s3source_object_info(options = {})
    end

    def overlay(options = {})
    end

    def redactions(options = {})
        []
    end

    def metadata(options = {})
    end
end