FROM uclalibrary/cantaloupe:5.0.4-1
# USER cantaloupe
# WORKDIR /home/cantaloupe
COPY --chown=cantaloupe:users ./lincs/* .
COPY --chown=cantaloupe:users ./delegates.rb /etc/delegates.rb
